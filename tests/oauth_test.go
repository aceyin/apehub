package tests

import (
	"testing"
	"encoding/json"
	"apehub/domain"
	"github.com/stretchr/testify/assert"
)

func Test_unmarshal_github_user(t *testing.T) {
	jsonStr := `{"login":"aceyin","id":482266,"node_id":"MDQ6VXNlcjQ4MjI2Ng==","avatar_url":"https://avatars0.githubusercontent.com/u/482266?v=4","gravatar_id":"","url":"https://api.github.com/users/aceyin","html_url":"https://github.com/aceyin","followers_url":"https://api.github.com/users/aceyin/followers","following_url":"https://api.github.com/users/aceyin/following{/other_user}","gists_url":"https://api.github.com/users/aceyin/gists{/gist_id}","starred_url":"https://api.github.com/users/aceyin/starred{/owner}{/repo}","subscriptions_url":"https://api.github.com/users/aceyin/subscriptions","organizations_url":"https://api.github.com/users/aceyin/orgs","repos_url":"https://api.github.com/users/aceyin/repos","events_url":"https://api.github.com/users/aceyin/events{/privacy}","received_events_url":"https://api.github.com/users/aceyin/received_events","type":"User","site_admin":false,"name":"Ace Yin","company":"un32.com","blog":"","location":"guangzhou","email":"aceyin@gmail.com","hireable":null,"bio":"programmer for golang, java, kotlin","public_repos":16,"public_gists":1,"followers":4,"following":4,"created_at":"2010-11-15T13:49:12Z","updated_at":"2018-09-06T10:10:23Z","private_gists":0,"total_private_repos":0,"owned_private_repos":0,"disk_usage":29135,"collaborators":0,"two_factor_authentication":false,"plan":{"name":"free","space":976562499,"collaborators":0,"private_repos":0}}`
	user := new(domain.GithubUser)
	e := json.Unmarshal([]byte(jsonStr), user)
	if e != nil {
		t.Fatal("unmarshal failed")
		println(e.Error())
		t.Fail()
	}

	ast := assert.New(t)
	ast.Equal("aceyin", user)

}
