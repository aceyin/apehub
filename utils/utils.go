package utils

import (
	"net/url"
	"crypto/md5"
	"time"
	"fmt"
	"math/rand"
	"encoding/base64"
	"strings"
)

// 计算页数
func PageCount(rowNum uint, pageSize uint) uint {
	var pages uint
	if rowNum%pageSize == 0 {
		pages = uint(rowNum / pageSize)
	} else {
		pages = uint(rowNum/pageSize + 1)
	}
	return pages
}

// 将传入的 map 构建成一个 URL 里面的 query string 格式
func BuildQueryString(keyValues map[string]string) string {
	if keyValues == nil || len(keyValues) == 0 {
		return ""
	}
	values := url.Values{}
	for k, v := range keyValues {
		values.Set(k, v)
	}
	return values.Encode()
}

// test if a string is nil or blank
func IsBlank(str string) bool {
	if str == "" || strings.TrimSpace(str) == "" {
		return true
	}
	return false
}

func UrlEncode(str string) (ret string) {
	return base64.URLEncoding.EncodeToString([]byte(str))
}

func UrlDecode(str string) (ret string) {
	return base64.URLEncoding.EncodeToString([]byte(str))
}

func Base64Encode(str string) string {
	return base64.StdEncoding.EncodeToString([]byte(str))
}

func Base64Decode(str string) ([]byte, error) {
	return base64.StdEncoding.DecodeString(str)
}

const letterBytes = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ"
const (
	letterIdxBits = 6                    // 6 bits to represent a letter index
	letterIdxMask = 1<<letterIdxBits - 1 // All 1-bits, as many as letterIdxBits
	letterIdxMax  = 63 / letterIdxBits   // # of letter indices fitting in 63 bits
)

// 随机生成字符串
// n 字符串长度
func Random(n int) string {
	var src = rand.NewSource(time.Now().UnixNano())
	b := make([]byte, n)
	// A src.Int63() generates 63 random bits, enough for letterIdxMax characters!
	for i, cache, remain := n-1, src.Int63(), letterIdxMax; i >= 0; {
		if remain == 0 {
			cache, remain = src.Int63(), letterIdxMax
		}
		if idx := int(cache & letterIdxMask); idx < len(letterBytes) {
			b[i] = letterBytes[idx]
			i--
		}
		cache >>= letterIdxBits
		remain--
	}

	return string(b)
}

// 加密给定的密码明文，返回加密之后的密码以及对这个密码进行加密的盐
func EncryptPassword(pwd string) (password string, salt string) {
	salt = fmt.Sprintf("%d", rand.NewSource(time.Now().UnixNano()).Int63())
	password = Encrypt(pwd, salt)
	return password, salt
}

// 进行加密
func Encrypt(text string, salt string) string {
	hash := md5.Sum([]byte(salt + text))
	return fmt.Sprintf("%X", hash)
}

// 检查密码是否正确
// pwd 原始密码
// salt 保存在数据库的盐
// encrypted 保存在数据库的加密过的密码
func VerifyPassword(pwd string, salt string, encrypted string) (success bool) {
	str := Encrypt(pwd, salt)
	return str == encrypted
}

// 获取给定日期的接下来的2个连续日期
// date 格式: YYYY-MM-DD
func NextTowDays(date string) (dateBegin time.Time, dateEnd time.Time, err error) {
	now, err := time.Parse("2006-01-02", date)
	if err != nil {
		return time.Now(), time.Now(), err
	}
	dateBegin = now.Add(time.Hour * 24)
	dateEnd = dateBegin.Add(time.Hour * 24)
	return dateBegin, dateEnd, nil
}

// 快速转化为日期
// str 日期字符串，符合 2006-02-01 格式
func ToDate(str string) (date time.Time, err error) {
	return time.Parse("2006-01-02", str)
}
