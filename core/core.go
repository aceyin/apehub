package core

import (
	"log"
)

var shutdownHooks = make(map[string]func())

func RegisterShutdownHook(name string, hook func()) {
	shutdownHooks[name] = hook
}

func CallShutdownHooks() {
	for name, hook := range shutdownHooks {
		log.Println("Calling shutdown hook :", name)
		hook()
	}
}
