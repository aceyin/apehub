package logs

import (
	"go.uber.org/zap"
	"go.uber.org/zap/zapcore"
	"apehub/conf"
	"os"
	"apehub/core"
	"time"
	"log"
)

var logger *zap.SugaredLogger

// package init
func init() {
	cfg := zap.NewProductionConfig()
	cfg.DisableCaller = true
	cfg.EncoderConfig.LevelKey = conf.Logs.Level
	cfg.EncoderConfig.TimeKey = "timestamp"
	cfg.EncoderConfig.EncodeTime = TimeEncoder
	if conf.Logs.Target == "file" {
		cfg.OutputPaths = []string{conf.Logs.File}
	} else {
		zapcore.AddSync(os.Stdout)
	}

	if conf.Logs.Format == "json" {
		cfg.Encoding = "json"
	} else {
		cfg.Encoding = "console"
	}

	l, err := cfg.Build()
	if err != nil {
		panic(err)
	} else {
		logger = l.Sugar()
	}

	core.RegisterShutdownHook("logger-flush-hook", func() {
		log.Fatal("Shutting down::: Flushing log to files")
		logger.Sync()
	})
}

func TimeEncoder(t time.Time, enc zapcore.PrimitiveArrayEncoder) {
	enc.AppendString(t.Format("2006/01/02 15:04:05.000"))
}

func Debug(msg string, params ...interface{}) {
	if params == nil {
		logger.Debug(msg)
	} else {
		logger.Debugf(msg, params...)
	}
}

func Info(msg string, params ...interface{}) {
	if params == nil {
		logger.Info(msg)
	} else {
		logger.Infof(msg, params...)
	}
}

func Warn(msg string, params ...interface{}) {
	if params == nil {
		logger.Warn(msg)
	} else {
		logger.Warnf(msg, params...)
	}
}

// 注意: Error, Panic, Fatal 三个方法更适合于系统内抛异常了之后打印日志使用。
// 因为这3个方法输出的日志，会将 call stacktrace 也打印出来
// 如果只是系统内的一些逻辑错误，如数据判断时出错等，推荐使用 Warn 来输出日志
func Error(msg string, params ...interface{}) {
	if params == nil {
		logger.Error(msg)
	} else {
		logger.Errorf(msg, params...)
	}
}

func Panic(msg string, params ...interface{}) {
	if params == nil {
		logger.Panic(msg)
	} else {
		logger.Panicf(msg, params...)
	}
}

func Fatal(msg string, params ...interface{}) {
	if params == nil {
		logger.Fatal(msg)
	} else {
		logger.Fatalf(msg, params...)
	}
}
