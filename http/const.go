package http

type mediaType string

var MediaTypes = struct {
	Json mediaType
	Xml  mediaType
	Html mediaType
}{
	Json: "application/json",
	Xml:  "text/xml",
	Html: "text/html",
}

type contentType string

var ContentTypes = struct {
	Json      contentType
	Form      contentType
	Multipart contentType
}{
	Json:      "application/json",
	Form:      "application/x-www-form-urlencoded",
	Multipart: "multipart/form-data",
}
