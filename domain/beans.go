package domain

// 从 github 获取信息是返回的错误
// 参考 docs/github-error.json
type GithubError struct {
	Message          string
	DocumentationUrl string `json:"documentation_url"`
}

type GithubRepoSearchResult struct {
	TotalCount        uint         `json:"total_count"`
	IncompleteResults bool         `json:"incomplete_results"`
	Items             []GithubRepo `json:"items"`
}

// 从 github 获取到的 repo 信息
// 参考 docs/github-repo.json

type UserCookie struct {
	Id       uint   `json:"i"`
	Name     string `json:"n"`
	Username string `json:"u"`
	Avatar   string `json:"a"`
	Lang     string `json:"l"`
	Token    string `json:"t"`
}
