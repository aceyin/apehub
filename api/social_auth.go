package api

import (
	"apehub/conf"
	"apehub/domain"
	"apehub/http"
	"apehub/logs"
	"apehub/resp"
	"apehub/service"
	"encoding/json"
	"fmt"
	"github.com/gin-gonic/gin"
	"math/rand"
	"strings"
)

var SocialAuth = struct {
	RedirectToOpenApi      func(ctx *gin.Context)
	ReceiveOpenApiCallback func(ctx *gin.Context)
}{
	// 1. oauth 重定向接口，组装参数，重定向到目标网站
	RedirectToOpenApi: func(ctx *gin.Context) {
		name := ctx.Param("name")
		oauth := conf.Oauth.Get(name)
		if oauth == nil {
			logs.Warn("Invalid request: no oauth provider name")
			abort(ctx, resp.BadRequest("Invalid request: no oauth provider name"))
			return
		}

		switch oauth.Name {
		case "github":
			redirect(ctx, oauth.AuthUrl, buildGithubAuthorizeParams(oauth))
			return
			// TODO 处理其他 oauth 网站的请求
		default:
			logs.Warn("Invalid callback request: unknown oauth provider name")
			abort(ctx, resp.BadRequest("Invalid callback request: unknown oauth provider name"))
			return
		}
	},

	// 2. oauth 回调接口，传回用于获取 access_token 的 code
	// 3. 然后再用获取到的 access_token 调用用户资料接口，获取用户信息
	ReceiveOpenApiCallback: func(ctx *gin.Context) {
		logs.Info("Receive oauth callback from:%v", ctx.GetHeader("referrer"))

		name := ctx.Param("name")
		oauth := conf.Oauth.Get(name)
		if oauth == nil {
			logs.Warn("Invalid callback request: unknown oauth provider name")
			abort(ctx, resp.BadRequest("Invalid callback request: unknown oauth provider name"))
			return
		}

		switch oauth.Name {
		case "github":
			handleGithubCallback(oauth, ctx)
			return
			// TODO 处理其他 oauth 网站的请求
		default:
			logs.Warn("Invalid callback request: unknown oauth provider name")
			abort(ctx, resp.BadRequest("Invalid callback request: unknown oauth provider name"))
			return
		}
	},
}

// 根据 oauth 的配置，构建一个适用于 github 的 authorize url
func buildGithubAuthorizeParams(setting *conf.OauthConfig) map[string]string {
	return map[string]string{
		"client_id": setting.ClientId,
		//"redirect_url": setting.CallbackUrl,
		"scope": strings.Join(*setting.Scopes, " "),
		"state": fmt.Sprintf("%.10f", rand.Float64()),
	}
}

// 处理 oauth 的回调请求:
// 1. 从回调参数中，获取临时用的 code
// 2. 将临时用的code随同client_id等一起 POST 到access_token接口
// 3. 从第二步返回的数据中获取 access_token
// 4. 利用 access_token 调用获取用户信息接口，获取用户信息
func handleGithubCallback(setting *conf.OauthConfig, ctx *gin.Context) {
	logs.Info("Handle github oauth callback ...")
	// 1. get code
	code := ctx.Query("code")
	if code == "" {
		abort(ctx, resp.BadRequest("No 'code' parameter present in callback url"))
		return
	}
	// TODO 检查 state 与之前传递给 github 的是否一致，防止伪造攻击
	state := ctx.Query("state")
	logs.Debug("State returned from github is :%v", state)

	// 2. call github access_token api and parse the access_token from the api response
	data := map[string]string{
		"client_id":     setting.ClientId,
		"client_secret": setting.Secret,
		"code":          code,
		"state":         state,
	}
	url := setting.AccessTokenUrl
	logs.Info("Trying to get access token from github %v", url)
	_, str, e := http.Util.Post(
		url,
		true,
		http.RequestOption{
			ContentType: http.ContentTypes.Form,
			Accept:      http.MediaTypes.Json,
			Data:        data,
		})
	if e != nil {
		msg := fmt.Sprintf("Error while call github oauth api: %v, error is:%v", url, e.Error())
		logs.Warn(msg)
		abort(ctx, resp.ServerError(msg))
		return
	}

	// 3. use the access_token to get user profile
	token := new(accessTokenResponse)
	if e = json.Unmarshal([]byte(str), token); e != nil {
		msg := fmt.Sprintf("Error while parse github access token response to JSON: %v", e.Error())
		logs.Warn(msg)
		abort(ctx, resp.ServerError(msg))
		return
	}

	headers := map[string]string{
		"Authorization": " token " + token.Token,
	}

	logs.Info("Trying to get user info from github %v", setting.ProfileUrl)
	status, prof, er := http.Util.Get(
		setting.ProfileUrl+"?access_token="+token.Token,
		true,
		http.RequestOption{
			Headers: headers,
		})
	if er != nil {
		msg := fmt.Sprintf("Error while get github user profile: %v", er.Error())
		logs.Warn(msg)
		abort(ctx, resp.ServerError(msg))
		return
	}

	// 4. get user profile from github
	// 如果返回的状态码不是 200 则报错
	if status != 200 {
		var tip = "--"
		errors := domain.GithubError{}
		ex := json.Unmarshal([]byte(prof), &errors)
		if ex == nil {
			tip = errors.Message
		}
		msg := fmt.Sprintf("Error while get github user profile, status=%v, message = %v", status, tip)
		logs.Warn(msg)
		abort(ctx, resp.ServerError(msg))
		return
	}

	gitUser := new(domain.GithubUser)
	logs.Info("Get user profile from github: status = %v, body = %v", status, prof)
	if e = json.Unmarshal([]byte(prof), gitUser); e != nil {
		msg := fmt.Sprintf("Error while unmarshal github user info: %v", e.Error())
		logs.Warn(msg)
		abort(ctx, resp.ServerError(msg))
		return
	}

	// 5. 如果当前用户请求中有cookie，则把 github 账号与当前账号绑定
	user, valid, _ := readUserCookie(ctx)
	var res *resp.Result
	if valid && user != nil {
		res = service.UserService.BindGithubAccount(user.Id, gitUser)
	} else {
		//6. 否则用github账号创建新的用户
		res = service.UserService.CreateUserWithGithub(gitUser)
	}

	if !res.Succeed() {
		abort(ctx, res)
		return
	}

	// 6. login user
	ctx.Set(KEY_OAUTH_USER_INFO, res.Data)
	forward(ctx, Login.LoginHandler)
}

type accessTokenResponse struct {
	Token string `json:"access_token"`
	Scope string `json:"scope"`
	//TokenType string `json:"token_type"`
}
