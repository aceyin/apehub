package sql

type Xql string

func (s Xql) Build(param map[string]interface{}) string {
	if param == nil || len(param) == 0 {
		return string(s)
	}
	return ""
}
