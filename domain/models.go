package domain

import (
	"apehub/consts"
	"time"
)

type Model struct {
	ID        uint       `gorm:"primary_key"`
	CreatedAt time.Time  `gorm:"created_at" json:"created_at"`
	UpdatedAt time.Time  `gorm:"created_at" json:"created_at"`
	DeletedAt *time.Time `sql:"index"`
}

type User struct {
	Model
	Username         string            `gorm:"type:varchar(100);unique_index"` // 用户名，登录用
	Email            string            `gorm:"type:varchar(100);unique_index"` // email
	Mobile           string            `gorm:"type:varchar(20);unique_index"`  // mobile
	Name             string            `gorm:"type:varchar(100)"`              // 昵称
	Avatar           string            `gorm:"type:varchar(255)"`              // 头像
	Password         string            `gorm:"type:varchar(40)" json:"-"`      // 密码
	Salt             string            `grom:"type:varchar(40)" json:"-"`      // 盐
	Lang             string            `gorm:"type:varchar(50)"`               // 语言代码，如 zh_CN
	ThirdPartyUserId string            `gorm:"type:varchar(100)"`              // 第三方网站的用户id
	RegisterFrom     string            `gorm:"type:varchar(40)"`               // 注册来源，如 github
	Status           consts.UserStatus `gorm:"type:varchar(20)"`               // 状态，参考 const.UserStatus
}

type GithubUser struct {
	ID                uint       `gorm:"-" json:"-"` // 对应到数据库自增长的id，不需要从json解析出来
	CreatedAt         time.Time  `gorm:"created_at" json:"created_at"`
	UpdatedAt         time.Time  `gorm:"created_at" json:"created_at"`
	DeletedAt         *time.Time `sql:"index"`
	Login             string     `json:"login"`
	UserId            uint       `gorm:"user_id" json:"id"` // 对应到 github 返回的 json 中的 ID, 存到数据库的 user_id 字段
	AvatarURL         string     `gorm:"avatar_url" json:"avatar_url"`
	GravatarID        string     `gorm:"-" json:"gravatar_id"`
	URL               string     `gorm:"url" json:"url"`
	HTMLURL           string     `gorm:"html_url" json:"html_url"`
	FollowersURL      string     `gorm:"followers_url" json:"followers_url"`
	FollowingURL      string     `gorm:"following_url" json:"following_url"`
	GistsURL          string     `gorm:"gists_url" json:"gists_url"`
	StarredURL        string     `gorm:"starred_url" json:"starred_url"`
	SubscriptionsURL  string     `gorm:"subscriptions_url" json:"subscriptions_url"`
	OrganizationsURL  string     `gorm:"organizations_url" json:"organizations_url"`
	ReposURL          string     `gorm:"repos_url" json:"repos_url"`
	EventsURL         string     `gorm:"events_url" json:"events_url"`
	ReceivedEventsURL string     `gorm:"received_events_url" json:"received_events_url"`
	Type              string     `gorm:"type" json:"type"`
	SiteAdmin         bool       `gorm:"site_admin" json:"site_admin"`
	Name              string     `gorm:"name" json:"name"`
	Company           string     `gorm:"company" json:"company"`
	Blog              string     `gorm:"blog" json:"blog"`
	Location          string     `gorm:"location" json:"location"`
	Email             string     `gorm:"email" json:"email"`
	Bio               string     `gorm:"bio" json:"bio"`
	PublicRepos       uint       `gorm:"public_repos" json:"public_repos"`
	PublicGists       uint       `gorm:"public_gists" json:"public_gists"`
	Followers         uint       `gorm:"followers" json:"followers"`
	Following         uint       `gorm:"following" json:"following"`
	LinkedUser        uint       `gorm:"linked_user" json:"-"`
}

type GithubRepo struct {
	ID               uint       `gorm:"primary_key" json:"-"`
	CreatedAt        time.Time  `gorm:"created_at" json:"created_at"`
	UpdatedAt        time.Time  `gorm:"created_at" json:"created_at"`
	DeletedAt        *time.Time `gorm:"deleted_at" sql:"index"`
	RepoId           uint       `gorm:"repo_id" json:"id"`
	NodeID           string     `gorm:"-" json:"node_id"`
	Name             string     `gorm:"name" json:"name"`
	FullName         string     `gorm:"full_name" json:"full_name"`
	Owner            GithubUser `gorm:"-" json:"owner"`
	OwnerId          uint       `gorm:"owner_id" json:"-"`
	Category         string     `gorm:"category" json:"-"`
	Private          bool       `gorm:"-" json:"private"`
	HTMLURL          string     `gorm:"html_url" json:"html_url"`
	Description      string     `gorm:"description" json:"description"`
	Fork             bool       `gorm:"fork" json:"fork"`
	URL              string     `gorm:"url" json:"url"`
	ForksURL         string     `gorm:"forks_url" json:"forks_url"`
	KeysURL          string     `gorm:"-" json:"keys_url"`
	CollaboratorsURL string     `gorm:"collaborators_url" json:"collaborators_url"`
	TeamsURL         string     `gorm:"teams_url" json:"teams_url"`
	HooksURL         string     `gorm:"-" json:"hooks_url"`
	IssueEventsURL   string     `gorm:"-" json:"issue_events_url"`
	EventsURL        string     `gorm:"-" json:"events_url"`
	AssigneesURL     string     `gorm:"assignees_url" json:"assignees_url"`
	BranchesURL      string     `gorm:"branches_url" json:"branches_url"`
	TagsURL          string     `gorm:"tags_url" json:"tags_url"`
	BlobsURL         string     `gorm:"blobs_url" json:"blobs_url"`
	GitTagsURL       string     `gorm:"-" json:"git_tags_url"`
	GitRefsURL       string     `gorm:"-" json:"git_refs_url"`
	TreesURL         string     `gorm:"-" json:"trees_url"`
	StatusesURL      string     `gorm:"statuses_url" json:"statuses_url"`
	LanguagesURL     string     `gorm:"languages_url" json:"languages_url"`
	StargazersURL    string     `gorm:"stargazers_url" json:"stargazers_url"`
	ContributorsURL  string     `gorm:"contributors_url" json:"contributors_url"`
	SubscribersURL   string     `gorm:"subscribers_url" json:"subscribers_url"`
	SubscriptionURL  string     `gorm:"subscription_url" json:"subscription_url"`
	CommitsURL       string     `gorm:"commits_url" json:"commits_url"`
	GitCommitsURL    string     `gorm:"-" json:"git_commits_url"`
	CommentsURL      string     `gorm:"comments_url" json:"comments_url"`
	IssueCommentURL  string     `gorm:"issue_comment_url" json:"issue_comment_url"`
	ContentsURL      string     `gorm:"-" json:"contents_url"`
	CompareURL       string     `gorm:"-" json:"compare_url"`
	MergesURL        string     `gorm:"-" json:"merges_url"`
	ArchiveURL       string     `gorm:"-" json:"archive_url"`
	DownloadsURL     string     `gorm:"downloads_url" json:"downloads_url"`
	IssuesURL        string     `gorm:"issues_url" json:"issues_url"`
	PullsURL         string     `gorm:"pulls_url" json:"pulls_url"`
	MilestonesURL    string     `gorm:"-" json:"milestones_url"`
	NotificationsURL string     `gorm:"-" json:"notifications_url"`
	LabelsURL        string     `gorm:"labels_url" json:"labels_url"`
	ReleasesURL      string     `gorm:"releases_url" json:"releases_url"`
	DeploymentsURL   string     `gorm:"deployments_url" json:"deployments_url"`
	PushedAt         time.Time  `gorm:"pushed_at" json:"pushed_at"`
	GitURL           string     `gorm:"git_url" json:"git_url"`
	SSHURL           string     `gorm:"-" json:"ssh_url"`
	CloneURL         string     `gorm:"-" json:"clone_url"`
	SvnURL           string     `gorm:"-" json:"svn_url"`
	Homepage         string     `gorm:"homepage" json:"homepage"`
	Size             uint       `gorm:"size" json:"size"`
	StargazersCount  uint       `gorm:"stargazers_count" json:"stargazers_count"`
	WatchersCount    uint       `gorm:"watchers_count" json:"watchers_count"`
	Language         string     `gorm:"language" json:"language"`
	HasIssues        bool       `gorm:"-" json:"has_issues"`
	HasProjects      bool       `gorm:"-" json:"has_projects"`
	HasDownloads     bool       `gorm:"-" json:"has_downloads"`
	HasWiki          bool       `gorm:"-" json:"has_wiki"`
	HasPages         bool       `gorm:"-" json:"has_pages"`
	ForksCount       uint       `gorm:"forks_count" json:"forks_count"`
	MirrorURL        string     `gorm:"-" json:"mirror_url"`
	Archived         bool       `gorm:"-" json:"archived"`
	OpenIssuesCount  uint       `gorm:"open_issues_count" json:"open_issues_count"`
	LicenseName      string     `gorm:"license_name" json:"-"`
	License struct {
		Key    string `json:"key"`
		Name   string `json:"name"`
		SpdxId string `json:"spdx_id"`
		URL    string `json:"url"`
		NodeId string `json:"-"`
	} `gorm:"-" json:"license"`
	Forks         uint   `gorm:"-" json:"forks"`
	OpenIssues    uint   `gorm:"-" json:"open_issues"`
	Watchers      uint   `gorm:"-" json:"watchers"`
	DefaultBranch string `gorm:"-" json:"default_branch"`
}

type Sequences struct {
	Name  string
	Value string
}

type Equipment struct {
	Model
	CateId     uint
	BrandId    uint
	Name       string
	Models     string
	Attributes string
}

type EquipCategory struct {
	Model
	Name string
	Icon string
}

type EquipBrand struct {
	Model
	Name string
	Logo string
}

type EquipBrandCategory struct {
	Model
	BrandId uint
	CateId  uint
}
