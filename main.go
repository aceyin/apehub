package main

import (
	"apehub/conf"
	"strconv"
	"os"
	"os/signal"
	"time"
	stdContext "context"
	"apehub/core"
	"apehub/routing"
	"log"
	"github.com/gin-gonic/gin"
	"github.com/gin-contrib/static"
	"net/http"
)

// for goreleaser tool
var (
	version     = "dev"
	commit      = "none"
	date        = "unknown"
	defaultPort = 8080
)

func main() {
	log.Println("Starting app (version:", version, ", build date:", date, ", commit:", commit, ")")

	engine := gin.Default()

	// register routers
	registerRouters(engine)

	// static file
	engine.Use(static.Serve("/", static.LocalFile("./static", true)))

	// register template path
	//engine.LoadHTMLGlob("./templates/*")

	//engine.Use(core.ResponseMiddleware)

	// start iris web server
	start(engine)
}

// 将 routing 里面的路由定义注册到 iris app
func registerRouters(app *gin.Engine) {
	for name, endpoints := range routing.Routers {
		// TODO 不是所有的都要 CookieMiddleware, 去掉那些不要的
		group := app.Group(name, core.CookieMiddleware())
		for _, endpoint := range endpoints {
			handler := endpoint.Handler
			if endpoint.Get != "" {
				group.GET(endpoint.Get, handler)
			}
			if endpoint.Post != "" {
				group.POST(endpoint.Post, handler)
			}
			if endpoint.Delete != "" {
				group.DELETE(endpoint.Delete, handler)
			}
			if endpoint.Head != "" {
				group.HEAD(endpoint.Head, handler)
			}
			if endpoint.Option != "" {
				group.OPTIONS(endpoint.Option, handler)
			}
			if endpoint.Put != "" {
				group.PUT(endpoint.Put, handler)
			}
		}

	}
	// 注册统一的错误处理
	//app.OnErrorCode(iris.StatusInternalServerError, api.Errors.ErrorHandler)
	//app.OnErrorCode(iris.StatusBadRequest, api.Errors.ErrorHandler)
}

func start(app *gin.Engine) {
	port := conf.App.Port
	if port <= 0 {
		port = defaultPort
	}

	addr := ":" + strconv.Itoa(port)

	server := &http.Server{
		Addr:    addr,
		Handler: app,
	}

	go func() {
		// service connections
		if err := server.ListenAndServe(); err != nil && err != http.ErrServerClosed {
			log.Fatalf("listen: %v\n", err.Error())
		}
	}()

	// enable graceful shutdown
	quit := make(chan os.Signal)
	signal.Notify(quit, os.Interrupt)
	<-quit
	log.Println("Shutdown Server ...")

	core.CallShutdownHooks()

	ctx, cancel := stdContext.WithTimeout(stdContext.Background(), 5*time.Second)
	defer cancel()
	if err := server.Shutdown(ctx); err != nil {
		log.Fatal("Server Shutdown:", err)
	}
	log.Println("Server exiting")
}
