package api

import (
	"github.com/gin-gonic/gin"
	"apehub/domain"
	"apehub/resp"
	"apehub/logs"
	"fmt"
)

var Login = struct {
	LoginHandler func(c *gin.Context)
}{
	LoginHandler: func(c *gin.Context) {
		// 如果是从 oauth 自动登录跳转过来的，则直接根据传递过来的用户信息完成登录
		if oauthUser, exists := c.Get(KEY_OAUTH_USER_INFO); exists && oauthUser != nil {
			user := oauthUser.(domain.User)
			logs.Info("Auto login from openid login redirect: %v", user.RegisterFrom)
			var token string
			var err error
			if token, err = genJwtToken(&user); err != nil {
				abort(c, resp.ServerError(err.Error()))
				return
			}

			setCookie(c, &user, token)
			jsonfy(c, *resp.Success("success github", &user))
			return
		}
		// 登录
		form := domain.LoginForm{}

		if err := c.ShouldBind(form); err != nil {
			msg := fmt.Sprintf("Error while binding login form:%v", err.Error())
			logs.Warn(msg)
			abort(c, resp.Fail(msg, nil))
		}

		jsonfy(c, *resp.Success("success", nil))
	},
}
