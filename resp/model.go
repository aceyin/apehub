package resp

type RespStatus int

const (
	FailStatus    RespStatus = iota
	SuccessStatus
)

// api 返回值的统一格式
type Result struct {
	Code    int         `json:"code"`
	Status  RespStatus  `json:"status"`
	Message string      `json:"message"`
	Data    interface{} `json:"data"`
}

// if succeed
func (this *Result) Succeed() bool {
	return this.Status == SuccessStatus
}

func Success(msg string, data interface{}) *Result {
	return &Result{
		Status:  SuccessStatus,
		Code:    200,
		Message: msg,
		Data:    data,
	}
}

func Fail(msg string, data interface{}) *Result {
	return &Result{
		Status:  FailStatus,
		Code:    200,
		Message: msg,
		Data:    data,
	}
}

// 创建一个 not found 的 resp
func NotFound(msg string) *Result {
	return &Result{
		Code:    404,
		Status:  FailStatus,
		Message: msg,
		Data:    nil,
	}
}

func BadRequest(msg string) *Result {
	return &Result{
		Code:    400,
		Status:  FailStatus,
		Message: msg,
		Data:    nil,
	}
}

func ServerError(msg string) *Result {
	return &Result{
		Code:    500,
		Status:  FailStatus,
		Message: msg,
		Data:    nil,
	}
}
