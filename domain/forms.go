package domain

type LoginForm struct {
	Username string `json:"username" bidding:"required"`
	Password string `json:"password" bidding:"required"`
}
