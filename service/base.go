package service

import (
	"apehub/conf"
	"github.com/jinzhu/gorm"
	"log"
	"os"
	"apehub/core"
	_ "github.com/go-sql-driver/mysql"
	"apehub/logs"
)

var database *gorm.DB

// init the service package
func init() {
	var err error
	ds := conf.DataSource
	if ds != nil {
		log.Println("Connect to database '", ds.Name, "' ...")
		if database, err = gorm.Open(ds.Type, ds.Url); err != nil {
			log.Fatal("Error while connect to database name='", ds.Name, "', type='", ds.Type, "' , url='", ds.Url, "', error:", err.Error())
			os.Exit(1)
		}
		database.LogMode(conf.Logs.SqlLog)
	}

	// 注: 使用 gorm 的时候 db.Model(User{}) 默认会 使用复数形式 (也就是表的名字会变成 users)
	// 如果要禁用复数形式，请打开下面这一行
	// database.SingularTable(true)

	core.RegisterShutdownHook("close-database-conn", func() {
		log.Fatal("Shutting down::: Closing database connections")
		database.Close()
	})
}

// 在事务内完成同一个操作
func transactional(action func(t *gorm.DB)) error {
	tx := database.Begin()
	//defer func() {
	//	if r := recover(); r != nil {
	//		tx.Rollback()
	//	}
	//}()
	// call operations
	action(tx)
	err := tx.Error
	if err != nil {
		tx.Rollback()
		return tx.Error
	}

	return tx.Commit().Error
}

func rollbackTransaction(tx *gorm.DB, err error) {
	logs.Warn("Rollback database transaction, %v", err.Error())
	tx.Rollback()
}
