package consts

// 用户状态
type UserStatus string

const (
	Normal     UserStatus = "normal"     // 正常状态
	Unverified UserStatus = "unverified" // 未验证
	Locked     UserStatus = "locked"     // 锁定
)

const (
	UserCookieName = "user"
	CookieMaxAge   = 7 * 24 * 3600
	CookieDomain   = "apehub.io"
	CookiePath     = "/"
)

const (
	Seq_github_repo_search_begin_date = "seq_github_repo_search_begin_date"
)
