package conf

import (
	"flag"
	"os"
	"path"
	"log"
	"github.com/BurntSushi/toml"
)

// app.toml wrapper
var appconfig = struct {
	Ds        dataSource `toml:"datasource"`
	Oauths    oauthList  `toml:"oauth"`
	App       app        `toml:"app"`
	Logs      logs       `toml:"logs"`
	CodeRepos []CodeRepo `toml:"code_repos"`
	DataDict  dataDict   `toml:"data_dict"`
}{}

// app.toml > data_dictionary
type dataDict struct {
	ProgramLanguages []string `toml:"program_languages"`
}

// app.toml > [code_hosting]
type CodeRepo struct {
	Name          string
	RepoSearchApi string `toml:"repo_search_api"`
}

// app.toml > [database] table
type dataSource = struct {
	Name string
	Type string
	Url  string
}

// app.toml > [[Oauth]]
type OauthConfig = struct {
	Enabled        bool
	Name           string
	ClientId       string `toml:"client_id"`
	Secret         string
	Scopes         *[]string
	AuthUrl        string `toml:"auth_url"`
	AccessTokenUrl string `toml:"token_url"`
	ProfileUrl     string `toml:"profile_url"`
}

type oauthList []OauthConfig

// 根据 OauthConfig 的 name 获取一个 OauthConfig 实例
func (this oauthList) Get(name string) *OauthConfig {
	for _, o := range this {
		if o.Name == name {
			return &o
		}
	}
	return nil
}

// 根据 OauthConfig 的 client_id 获取一个 OauthConfig 实例
func (this oauthList) GetByClientId(id string) *OauthConfig {
	for _, o := range this {
		if o.ClientId == id {
			return &o
		}
	}
	return nil
}

// app.toml > [app]
type app = struct {
	Name     string
	Port     int
	JwtSeeds string `toml:"jwt_seeds"`
}

// 生成 Jwt 的 secret
//func (this *app) JwtSecret() string {
//	// TODO 加密seeds
//	return this.JwtSeeds
//}

// app.toml > [logs]
type logs = struct {
	Level  string
	Format string
	Target string
	File   string
	SqlLog bool `toml:"sql_log"`
}

// alias for app.toml items
var DataSource = &appconfig.Ds
var Oauth = &appconfig.Oauths
var App = &appconfig.App
var Logs = &appconfig.Logs
var CodeRepos = &(appconfig.CodeRepos)
var DataDict = &(appconfig.DataDict)

// package init
func init() {
	defaultConf := "app.toml"

	// 命令行参数中指定的配置文件
	configPath := *(flag.String("c", "", "-c app.toml"))
	flag.Parse()

	// 从工作目录读取配置文件
	if len(configPath) == 0 {
		dir, err := os.Getwd()
		if err != nil {
			configPath = defaultConf
		} else {
			configPath = path.Join(dir, defaultConf)
		}
	}

	if len(configPath) == 0 {
		log.Fatal(
			`Cannot find app configuration file from current directory nor working directory.
Please run app with command line argument: -c <app.toml>.`)
		os.Exit(1)
	}

	_, e := toml.DecodeFile(configPath, &appconfig)
	if e != nil {
		log.Fatal("Error while parsing '", configPath, "', the error is:", e.Error())
		os.Exit(1)
	}
}
