package api

import (
	"apehub/logs"
	"github.com/gin-gonic/gin"
)

var Errors = struct {
	ErrorHandler func(ctx *gin.Context)
}{
	// 错误处理
	ErrorHandler: func(ctx *gin.Context) {
		code := ctx.Writer.Status()
		logs.Warn("Application run into error:%i", code)
		switch code {
		case 500:
			ctx.Redirect(500, "/500.html")
		case 400:
			ctx.Redirect(400, "/400.html")
		default:
			ctx.String(200, "")
		}
	},
}
