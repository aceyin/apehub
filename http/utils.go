package http

import (
	"strings"
	"net/http"
	"apehub/logs"
	"io/ioutil"
	"errors"
	"encoding/json"
	"bytes"
	"net/url"
	"time"
)

const (
	user_agent_chrome = "Mozilla/5.0 (Macintosh; Intel Mac OS X 10_13_6) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/69.0.3497.100 Safari/537.36"
)

type RequestOption struct {
	ContentType contentType       // the content type when the method is post and put
	Accept      mediaType         // the accept header
	Headers     map[string]string // the headers need to be send before call
	Data        map[string]string // the data to be send
	Timeout     uint              //timeout in seconds
}

func NewOption() *RequestOption {
	return &RequestOption{
		ContentType: "text/html",
		Accept:      MediaTypes.Html,
		Headers:     make(map[string]string),
		Data:        make(map[string]string),
		Timeout:     10,
	}
}

func (opt *RequestOption) WithTimeout(timeout uint) *RequestOption {
	opt.Timeout = timeout
	return opt
}

func (opt *RequestOption) MockChrome() *RequestOption {
	opt.Headers["User-Agent"] = user_agent_chrome
	return opt
}

func (opt *RequestOption) WithCookie(cookie string) *RequestOption {
	opt.Headers["Cookie"] = cookie
	return opt
}

func (opt *RequestOption) WithAccept(media mediaType) *RequestOption {
	opt.Headers["Accept"] = string(media)
	return opt
}

func (opt *RequestOption) WithContentType(t contentType) *RequestOption {
	opt.ContentType = t
	return opt
}

func (opt *RequestOption) WithHeader(key string, value string) *RequestOption {
	opt.Headers[key] = value
	return opt
}

func (opt *RequestOption) WithData(data map[string]string) *RequestOption {
	opt.Data = data
	return opt
}

// HTTP client instance
var client = &http.Client{}

// http util
var Util = struct {
	Get           func(url string, readResponse bool, option RequestOption) (status int, body string, err error)
	Post          func(url string, readResponse bool, option RequestOption) (status int, body string, err error)
	createRequest func(method string, url string, setting RequestOption) (*http.Request, error)
}{
	// send get request to the specified url
	// param readResponse - 是否需要读取返回的内容
	// return (http status, response body, error)
	// http status = 0 意味着调用接口失败
	Get: func(url string, readResponse bool, setting RequestOption) (int, string, error) {
		req, e := createRequest("GET", url, setting)
		if e != nil {
			return 0, "", e
		}
		return callRequest(req, readResponse)
	},
	// send post request
	// param readResponse - 是否需要读取返回的内容
	// return (http status, response body, error)
	Post: func(url string, readResponse bool, setting RequestOption) (int, string, error) {
		req, e := createRequest("POST", url, setting)
		if e != nil {
			return 0, "", e
		}
		return callRequest(req, readResponse)
	},
}

func callRequest(req *http.Request, readResponse bool) (int, string, error) {
	resp, er := client.Do(req)
	defer resp.Body.Close()
	status := resp.StatusCode
	if er != nil {
		logs.Warn("Error while send request to url: %v, error is: %v", req.URL.String(), er.Error())
		return status, "", er
	}
	if readResponse {
		b, e := ioutil.ReadAll(resp.Body)
		if e != nil {
			logs.Warn("Error while read response from url: %v, error is: %v", req.URL.String(), e.Error())
			return status, "", e
		}
		return status, string(b), nil
	}
	return status, "", nil
}

func createRequest(method string, uri string, option RequestOption) (*http.Request, error) {
	var req *http.Request
	var e error
	client.Timeout = time.Duration(option.Timeout) * time.Second

	switch strings.ToUpper(method) {
	case "GET":
		req, e = http.NewRequest(method, uri, nil)
		if e != nil {
			logs.Warn("Error while creating http client instance for url %v, error is: %v", uri, e.Error())
			return nil, e
		}

		if option.Data != nil && len(option.Data) > 0 {
			q := req.URL.Query()
			for k, v := range option.Data {
				q.Add(k, v)
			}
			req.URL.RawQuery = q.Encode()
		}
		break
	case "POST":
		if option.Data == nil || len(option.Data) == 0 {
			req, e = http.NewRequest(method, uri, nil)
			break
		}
		// compose post data according to different content type
		contentType := option.ContentType

		if contentType == ContentTypes.Json {
			jsn, e := json.Marshal(option.Data)
			if e != nil {
				logs.Warn("Error while convert data to JSON format: %v", e.Error())
				return nil, e
			}

			req, e = http.NewRequest(method, uri, bytes.NewBuffer(jsn))
		} else if contentType == ContentTypes.Form {
			values := url.Values{}
			for k, v := range option.Data {
				values.Set(k, v)
			}
			req, e = http.NewRequest(method, uri, bytes.NewBuffer([]byte(values.Encode())))
		} else {
			return nil, errors.New("Unsupported content type:" + string(contentType))
		}

		if e != nil {
			logs.Warn("Error while create http request for url: %v, error is: %v", uri, e.Error())
			return nil, e
		}
		req.Header.Set("Content-Type", string(contentType))
		break
	default:
		return nil, errors.New("Unsupported http method:" + method)
	}

	// set headers and accept
	if option.Accept != "" {
		req.Header.Set("Accept", string(option.Accept))
	}
	if option.Headers != nil && len(option.Headers) > 0 {
		for k, v := range option.Headers {
			req.Header.Set(k, v)
		}
	}

	return req, nil
}
