package modules

import (
	"apehub/conf"
	"apehub/http"
	"apehub/logs"
	"encoding/json"
	"apehub/domain"
	"sync"
	"apehub/utils"
	"fmt"
	"apehub/service"
	"time"
	"apehub/consts"
)

var lock = sync.Mutex{}
var cookie = "_octo=GH1.1.1594269501.1531981509; _ga=GA1.2.557525403.1531981511; logged_in=yes; dotcom_user=apehub; _gid=GA1.2.498908880.1538192045"
var opt = http.NewOption().WithAccept(http.MediaTypes.Json).MockChrome().
	WithCookie(cookie).WithHeader("Upgrade-Insecure-Requests", "1")

var CodeRepoFetcher = struct {
	FetchRepos func()
}{
	// fetch the repos from the app.toml > code_repos list
	FetchRepos: func() {
		lock.Lock()
		defer lock.Unlock()
		for _, repo := range *conf.CodeRepos {
			switch repo.Name {
			case "github":
				githubRepoFetcher.fetch(repo.RepoSearchApi)
			}
		}
	},
}

// github 的repo抓取类
var githubRepoFetcher = struct {
	fetch func(api string)
}{
	// 抓取 github 的 repositories
	// 注: 因为 github 对 search api 的诸多限制:
	// 限制1: 每分钟只能调用 search api 10次
	// 限制2: 每个IP调用同一个API最多只能获取 1000 条数据(不管分页多少次)
	// 限制3: 每次查询只能返回 100 个结果
	// 所以这里抓取数据的逻辑写的比较绕，希望按照以 created 日期为查询条件的方式，来绕过上述限制
	// 参考: https://stackoverflow.com/questions/37602893/github-search-limit-results
	fetch: func(api string) {
		logs.Info("Fetching GITHUB repository from api: %v", api)

		beginDateStr := service.SequenceService.Get(consts.Seq_github_repo_search_begin_date, "2008-02-01")

		beginDate, endDate, _ := utils.NextTowDays(beginDateStr)
		apiCallTimes := uint(0)
		totalRepoCount := uint(0)

		// 只要当前日期在 上次抓取日期 之后，就开始抓取
		for ; time.Now().After(beginDate); {
			searchDates := fmt.Sprintf("created:%v..%v", beginDate.Format("2006-01-02"), endDate.Format("2006-01-02"))

			// 拼接一个查询URL: https://api.github.com/search/repositories?q=created:2016-02-01..2016-02-02
			fetch_url := fmt.Sprintf("%v%v", api, searchDates)

			// 预抓取一次，获取记录条数级总页数
			result, stopFetch := doFetch(fetch_url)
			if result == nil || stopFetch {
				logs.Warn("Fetching github repos stopped because of github return bad result")
				return
			}
			apiCallTimes = apiCallTimes + 1

			repo_count := result.TotalCount
			page_size := uint(100)
			pages := utils.PageCount(repo_count, page_size)
			logs.Info("Found github repo from url: %v (repo count :%v, page count: %v)", fetch_url, repo_count, pages)

			for i := uint(1); i <= pages; {
				url := fmt.Sprintf("%v&page=%v&per_page=%v", fetch_url, i, page_size)
				logs.Info("Fetching github repos from url: %v, current page: %v/%v ", url, i, pages)
				result, stopFetch = doFetch(url)
				if stopFetch {
					logs.Warn("Fetching github repos stopped because of github return bad result")
					return
				}
				if result != nil {
					for _, repo := range result.Items {
						service.GithubService.SaveGithubRepo(&repo)
					}
					page_row_num := len(result.Items)
					totalRepoCount += uint(page_row_num)
					logs.Info("Finish save repos for page %v/%v, repos saved %v, total fetched repo: %v", i, pages, page_row_num, totalRepoCount)
				}
				i++
				apiCallTimes = apiCallTimes + 1
				// github 的search api 只允许每分钟调用 10次, 所以每10次调用，暂停62秒
				if apiCallTimes%10 == 0 {
					logs.Info("Pause 62 seconds before next round fetching ...")
					time.Sleep(62 * time.Second)
				}
			}
			// 更新日期
			// 抓取完成之后，更新数据库的下一次抓取起始日期
			service.SequenceService.Set(consts.Seq_github_repo_search_begin_date, endDate.Format("2006-01-02"))
			beginDate, endDate, _ = utils.NextTowDays(endDate.Format("2006-01-02"))
		}
	},
}

func doFetch(api string) (result *domain.GithubRepoSearchResult, shouldStop bool) {
	tryCountBeforeStop := uint(0)

	status, body, err := http.Util.Get(api, true, *opt)

	// 当出现 403 的时候，表示 github 又把接口给阻止了，这时候需要暂停2分钟
	for ; status == 403 && tryCountBeforeStop < 5; {
		logs.Warn("Github blocked our API %v, pause 2 min", api)
		time.Sleep(2 * time.Minute)
		status, body, err = http.Util.Get(api, true, *opt)
		tryCountBeforeStop = tryCountBeforeStop + 1
	}
	if status != 200 {
		logs.Warn("status=%v, body=%v,err=%v", status, body, err)
		logs.Warn("Wrong status while fetch github repos %v", api, status)
		return nil, true
	}
	results := domain.GithubRepoSearchResult{}
	err = json.Unmarshal([]byte(body), &results)
	if err != nil {
		logs.Warn("Error while unmarshal github repo search result, error:%v", err.Error())
		return nil, true
	} else {
		return &results, false
	}
}
