package tests

import (
	"testing"
	"fmt"
)

type I interface {
	walk()
}
type A struct {
	name string
}

func (a A) walk() {}

type B struct {
	name string
}

func (b B) walk() {}

func Test_golang_type_conversion(t *testing.T) {

	var i I
	i = A{name: "foo"}
	valA, okA := i.(A)
	fmt.Printf("%#v %#v\n", valA, okA)
	valB, okB := i.(B)
	fmt.Printf("%#v %#v\n", valB, okB)
}
