package service

import (
	"apehub/domain"
	"apehub/resp"
	"apehub/consts"
	"apehub/logs"
	"apehub/utils"
	"fmt"
)

var UserService = struct {
	CreateUserWithGithub func(guser *domain.GithubUser) *resp.Result
	BindGithubAccount    func(id uint, guser *domain.GithubUser) *resp.Result
}{
	// 使用github的用户信息创建新的账号
	CreateUserWithGithub: func(guser *domain.GithubUser) *resp.Result {
		return createUser(guser)
	},

	// 将现有账号与github绑定
	BindGithubAccount: func(userid uint, guser *domain.GithubUser) *resp.Result {
		user := domain.User{}
		database.First(&user, " id = ?", userid)

		if user.ID <= 0 {
			return createUser(guser)
		}

		guser.LinkedUser = user.ID
		create := database.FirstOrCreate(guser, "user_id = ? ", guser.UserId)
		if err := create.Error; err != nil {
			logs.Warn("Error while bind github user (id=%d) to user (id=%d)", guser.ID, userid)
		}
		// 绑定不成功也返回成功
		return resp.Success("", user)
	},
}

func createUser(guser *domain.GithubUser) *resp.Result {
	// transactional
	var user = domain.User{}
	// 1. 检查该用户是否已经绑定过了github账号
	database.Where("register_from=? and third_party_user_id =?", "github", guser.ID).First(&user)

	// 如果已经绑定过，则直接获取用户信息
	if user.ID > 0 {
		logs.Info("github user %v has registered before, direct login", guser.Login)
		guser.LinkedUser = user.ID
		database.Where("user_id=?", user.ID).FirstOrCreate(guser)
		return resp.Success("", user)
	}

	// 2. 如果没有绑定过，创建用户
	password, salt := utils.EncryptPassword(utils.Random(6))

	user = domain.User{
		Password:         password,
		Salt:             salt,
		Username:         guser.Login + "@github",
		Email:            guser.Email,
		Mobile:           "",
		Name:             guser.Name,
		Avatar:           guser.AvatarURL,
		RegisterFrom:     "github",
		Status:           consts.Normal,
		ThirdPartyUserId: fmt.Sprintf("%d", guser.ID),
	}
	tx := database.Begin()
	// query user or create one if user not exist
	if err := tx.Create(&user).Error; err != nil || user.ID == 0 {
		rollbackTransaction(tx, err)
		return resp.Fail("Error while save user:"+user.Username, nil)
	}

	guser.LinkedUser = user.ID

	if err := tx.Create(guser).Error; err != nil {
		rollbackTransaction(tx, err)
		return resp.Fail("Error while save github user: "+err.Error(), nil)
	}
	if err := tx.Commit().Error; err != nil {
		rollbackTransaction(tx, err)
		return resp.Fail("Error while commit transaction : "+err.Error(), nil)
	}

	logs.Info("Sync github user %v success", guser.Login)
	return resp.Success("", user)
}
