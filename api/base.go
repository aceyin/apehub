package api

import (
	"apehub/resp"
	"apehub/utils"
	"apehub/logs"
	"apehub/domain"
	"github.com/gin-gonic/gin"
	"github.com/dgrijalva/jwt-go"
	"time"
	"apehub/conf"
	"apehub/consts"
	"encoding/json"
	"apehub/errs"
)

const (
	KEY_OAUTH_USER_INFO = "KEY_OAUTH_USER_INFO"
)

// 直接显示错误页面
func abort(ctx *gin.Context, result *resp.Result) {
	//ctx.Status(200)
	//ctx.String(200, result.Message)
	ctx.AbortWithStatusJSON(200, result)
}

// handle json response
func jsonfy(ctx *gin.Context, result resp.Result) {
	//ctx.Status(200)
	//ctx.JSON(200, result)
	ctx.AbortWithStatusJSON(200, result)
}

// forward to another handler function
func forward(ctx *gin.Context, handler func(c *gin.Context)) {
	handler(ctx)
}

// redirect to the specified url by add the params as query string
func redirect(ctx *gin.Context, uri string, params map[string]string) {
	logs.Info("Redirecting to %v", uri)
	if params == nil || len(params) == 0 {
		ctx.Redirect(302, uri)
	} else {
		finalUrl := uri + "?" + utils.BuildQueryString(params)
		ctx.Redirect(302, finalUrl)
	}
}

func genJwtToken(user *domain.User) (string, error) {
	token := jwt.NewWithClaims(jwt.SigningMethodHS256, jwt.MapClaims{
		"id":  user.ID,
		"exp": time.Now().Add(time.Hour * 24 * 7).Unix(), // 可以添加过期时间
	})
	var str string
	var err error
	if str, err = token.SignedString([]byte(conf.App.JwtSeeds)); err != nil {
		logs.Warn("Error while creating JWT token:%v", err.Error())
		return "", err
	}
	logs.Debug("Generated JWT token for user %v, JWT=%v", user.Username, str)
	return str, nil
}

// 生成用户 cookie
// cookie 的value 保存了 UserCookie 的JSON字符串
func setCookie(ctx *gin.Context, user *domain.User, jwt string) {
	// set user cookie
	uc := domain.UserCookie{
		Id:       user.ID,
		Username: user.Username,
		Name:     user.Name,
		Avatar:   user.Avatar,
		Lang:     user.Lang,
		Token:    jwt,
	}
	var bts []byte
	var err error

	if bts, err = json.Marshal(uc); err != nil {
		abort(ctx, resp.ServerError("Error while marshal cookie value:"+err.Error()))
		return
	}

	value := utils.Base64Encode(string(bts))
	ctx.SetCookie(consts.UserCookieName, value, consts.CookieMaxAge, consts.CookiePath, consts.CookieDomain, false, false)
}

// 读取Cookie并判断cookie是否有效
func readUserCookie(ctx *gin.Context) (user *domain.UserCookie, valid bool, err error) {
	var value string
	if value, err = ctx.Cookie(consts.UserCookieName); err != nil {
		return nil, false, errs.InvalidCookie{Detail: err.Error()}
	}
	user = new(domain.UserCookie)
	jsn, err := utils.Base64Decode(value)
	if err != nil {
		return nil, false, errs.InvalidCookie{Detail: err.Error()}
	}

	if err = json.Unmarshal(jsn, user); err != nil {
		return nil, false, errs.InvalidCookie{Detail: err.Error()}
	}

	token, err := jwt.Parse(user.Token, func(token *jwt.Token) (interface{}, error) {
		if _, ok := token.Method.(*jwt.SigningMethodHMAC); !ok {
			return nil, errs.InvalidJwt{}
		}
		return []byte(conf.App.JwtSeeds), nil
	})

	if !token.Valid {
		logs.Warn("Invalid JWT token")
		return user, false, errs.InvalidJwt{}
	}

	return user, true, nil
}
