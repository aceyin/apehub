package errs

// 数据没找到错误
type NotFound struct {
	Detail string
}

func (e NotFound) Error() string {
	return e.Detail
}

// 无效的 JWT 错误
type InvalidJwt struct {
}

func (e InvalidJwt) Error() string {
	return "Invalid JWT token"
}

// 无效的Cookie
type InvalidCookie struct {
	Detail string
}

func (e InvalidCookie) Error() string {
	return "Invalid Cookie"
}
