package tests

import (
	"testing"
	"apehub/utils"
	assert2 "github.com/stretchr/testify/assert"
	"fmt"
	"apehub/http"
)

func Test_BuildQueryString(t *testing.T) {
	param := map[string]string{
		"a": "a",
		"b": "b",
		"c": "c",
	}

	str := utils.BuildQueryString(param)

	assert := assert2.New(t)
	assert.Equal("?a=a&b=b&c=c&", str)
}

func Test_encrypt_password(t *testing.T) {
	str := "1a2b3c"
	password, salt := utils.EncryptPassword(str)
	fmt.Printf("password = %v, salt = %v", password, salt)
}

func Test_encrypt(t *testing.T) {
	text := "1a2b3c"
	salt := "111111"
	str := utils.Encrypt(text, salt)
	println(str)

	str2 := utils.Encrypt(text, salt)
	println(str2)
}

func Test_Verify_password(t *testing.T) {
	origin := "1a2b3c"
	salt := "8841008178993465145"
	encrypted := "9431F4E13FD547C58B4768B30913611C"

	success := utils.VerifyPassword(origin, salt, encrypted)
	if !success {
		t.Error("密码不相等")
	}
}

func Test_ramdon(t *testing.T) {
	ran := utils.Random(6)
	println(ran)
}

func Test_pageCount(t *testing.T) {
	row_num := uint(56172)
	pages := utils.PageCount(row_num, 100)
	assert := assert2.New(t)
	println(pages)
	assert.Equal(uint(562), pages)
}

func Test_http_util(t *testing.T) {
	opt := http.NewOption().WithAccept("application/json").WithCookie("_octo=GH1.1.1594269501.1531981509; _ga=GA1.2.557525403.1531981511; logged_in=yes; dotcom_user=apehub; _gid=GA1.2.498908880.1538192045")
	for i := 0; i < 20; {
		url := fmt.Sprintf("https://api.github.com/search/repositories?q=stars:>=300&per_page=30&page=%v", i)
		status, body, err := http.Util.Get(url, true, *opt)
		fmt.Printf("%v::::status=%v, body=%v,err=%v\n", i, status, body, err)
		i++
	}
}

func Test_next2days(t *testing.T) {
	dateBegin, dateEnd, _ := utils.NextTowDays("2018-09-29")
	println(fmt.Sprintf("begin date:%v, end date: %v", dateBegin, dateEnd))
}
