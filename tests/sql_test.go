package tests

import (
	_ "github.com/go-sql-driver/mysql"
	"testing"
	"apehub/service"
)

func Test_sequences(t *testing.T) {
	get := service.SequenceService.Get("test-key", "test-value")
	println(get)

	service.SequenceService.Set("test-key", "new-test-value")

	service.SequenceService.Set("new-key", "new-value")
}
