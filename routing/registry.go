package routing

import "github.com/gin-gonic/gin"

// router Registry
type Registry map[string]endpoints

type endpoint struct {
	Get     string
	Post    string
	Put     string
	Head    string
	Delete  string
	Option  string
	Handler gin.HandlerFunc
}

type endpoints []endpoint
