package routing

import (
	"apehub/api"
	"apehub/api/admin"
)

// routers
var Routers = Registry{
	"login": {
		{
			Post:    "/",
			Handler: api.Login.LoginHandler,
		},
	},
	// 错误处理 api 接口
	"error": {
		{
			Get:     "/",
			Handler: api.Errors.ErrorHandler,
		},
	},
	// oauth 第三方登录的 api 接口
	"social": {
		{
			Get:     "/auth/redirect/:name",
			Handler: api.SocialAuth.RedirectToOpenApi,
		},
		{
			Get:     "/auth/callback/:name",
			Handler: api.SocialAuth.ReceiveOpenApiCallback,
		},
	},

	// admin
	"admin": {
		// github 接口
		{
			Get:     "/github/repos/fetch",
			Handler: admin.GithubApi.FetchRepos,
		},
	},
}
