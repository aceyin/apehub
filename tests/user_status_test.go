package tests

import (
	"testing"
	"apehub/domain"
	"apehub/consts"
	"github.com/stretchr/testify/assert"
)

func Test_user_status(t *testing.T) {
	asserts := assert.New(t)

	u := domain.User{
		Status: consts.Normal,
	}

	asserts.Equal(consts.Normal, u.Status, "用户状态不正确")

}
