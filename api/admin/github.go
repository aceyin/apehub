package admin

import (
	"github.com/gin-gonic/gin"
	"apehub/modules"
)

var GithubApi = struct {
	FetchRepos func(c *gin.Context)
}{
	FetchRepos: func(c *gin.Context) {
		go modules.CodeRepoFetcher.FetchRepos()
	},
}
