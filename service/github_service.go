package service

import (
	"apehub/domain"
	"apehub/resp"
	"apehub/utils"
)

var GithubService = struct {
	SaveGithubRepo func(repo *domain.GithubRepo) (result *resp.Result)
}{
	SaveGithubRepo: func(repo *domain.GithubRepo) (result *resp.Result) {
		//seq := domain.Sequences{}
		//database.First(&seq, "name = 'max-github-repo-id'")
		//if repo.RepoId > seq.Value {
		//	database.Model(&seq).Where("name= 'max-github-repo-id' ").Update("value", repo.RepoId)
		//}
		database.FirstOrCreate(repo, "repo_id = ?", repo.RepoId)
		owner := repo.Owner
		if utils.IsBlank(owner.AvatarURL) {
			owner.AvatarURL = owner.GravatarID
		}
		database.FirstOrCreate(&owner, "user_id = ?", owner.UserId)

		return resp.Success("", nil)
	},
}
