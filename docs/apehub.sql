/*
 Navicat Premium Data Transfer

 Source Server         : localhost
 Source Server Type    : MySQL
 Source Server Version : 50721
 Source Host           : localhost:3306
 Source Schema         : apehub

 Target Server Type    : MySQL
 Target Server Version : 50721
 File Encoding         : 65001

 Date: 26/09/2018 16:03:09
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for equip_brand_categorys
-- ----------------------------
DROP TABLE IF EXISTS `equip_brand_categorys`;
CREATE TABLE `equip_brand_categorys` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `brand_id` bigint(20) DEFAULT NULL,
  `cate_id` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=24 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_croatian_ci;

-- ----------------------------
-- Records of equip_brand_categorys
-- ----------------------------
BEGIN;
INSERT INTO `equip_brand_categorys` VALUES (1, '2018-09-18 14:45:04', '2018-09-18 14:45:06', NULL, 1, 1);
INSERT INTO `equip_brand_categorys` VALUES (2, '2018-09-18 14:45:20', '2018-09-18 14:45:22', NULL, 2, 1);
INSERT INTO `equip_brand_categorys` VALUES (3, '2018-09-18 14:45:35', '2018-09-18 14:45:37', NULL, 3, 1);
INSERT INTO `equip_brand_categorys` VALUES (4, '2018-09-18 14:45:45', '2018-09-18 14:45:47', NULL, 4, 1);
INSERT INTO `equip_brand_categorys` VALUES (5, '2018-09-18 14:45:58', '2018-09-18 14:46:01', NULL, 5, 1);
INSERT INTO `equip_brand_categorys` VALUES (6, '2018-09-18 14:46:10', '2018-09-18 14:46:12', NULL, 6, 1);
INSERT INTO `equip_brand_categorys` VALUES (7, '2018-09-18 14:46:22', '2018-09-18 14:46:24', NULL, 7, 1);
INSERT INTO `equip_brand_categorys` VALUES (8, '2018-09-18 14:46:32', '2018-09-18 14:46:34', NULL, 8, 1);
INSERT INTO `equip_brand_categorys` VALUES (9, '2018-09-18 14:46:43', '2018-09-18 14:46:46', NULL, 9, 1);
INSERT INTO `equip_brand_categorys` VALUES (10, '2018-09-18 14:46:53', '2018-09-18 14:46:55', NULL, 10, 1);
INSERT INTO `equip_brand_categorys` VALUES (11, '2018-09-18 15:22:49', '2018-09-18 15:22:52', NULL, 11, 2);
INSERT INTO `equip_brand_categorys` VALUES (12, '2018-09-18 15:24:20', '2018-09-18 15:24:22', NULL, 12, 2);
INSERT INTO `equip_brand_categorys` VALUES (13, '2018-09-18 15:24:32', '2018-09-18 15:24:34', NULL, 13, 2);
INSERT INTO `equip_brand_categorys` VALUES (14, '2018-09-18 15:24:44', '2018-09-18 15:24:46', NULL, 14, 2);
INSERT INTO `equip_brand_categorys` VALUES (15, '2018-09-18 15:24:52', '2018-09-18 15:24:54', NULL, 15, 2);
INSERT INTO `equip_brand_categorys` VALUES (16, '2018-09-18 15:25:00', '2018-09-18 15:25:03', NULL, 16, 2);
INSERT INTO `equip_brand_categorys` VALUES (17, '2018-09-18 15:25:15', '2018-09-18 15:25:17', NULL, 17, 2);
INSERT INTO `equip_brand_categorys` VALUES (18, '2018-09-18 15:29:56', '2018-09-18 15:29:59', NULL, 18, 2);
INSERT INTO `equip_brand_categorys` VALUES (19, '2018-09-18 15:30:10', '2018-09-18 15:30:12', NULL, 19, 2);
INSERT INTO `equip_brand_categorys` VALUES (20, '2018-09-18 15:30:25', '2018-09-18 15:30:28', NULL, 20, 2);
INSERT INTO `equip_brand_categorys` VALUES (21, '2018-09-18 15:30:37', '2018-09-18 15:30:39', NULL, 21, 2);
INSERT INTO `equip_brand_categorys` VALUES (22, '2018-09-18 15:31:52', '2018-09-18 15:31:54', NULL, 22, 2);
INSERT INTO `equip_brand_categorys` VALUES (23, '2018-09-18 15:32:22', '2018-09-18 15:32:24', NULL, 23, 2);
COMMIT;

-- ----------------------------
-- Table structure for equip_brands
-- ----------------------------
DROP TABLE IF EXISTS `equip_brands`;
CREATE TABLE `equip_brands` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `created_at` varchar(32) COLLATE utf8mb4_croatian_ci DEFAULT NULL,
  `updated_at` varchar(32) COLLATE utf8mb4_croatian_ci DEFAULT NULL,
  `deleted_at` varchar(32) COLLATE utf8mb4_croatian_ci DEFAULT NULL,
  `name` varchar(50) COLLATE utf8mb4_croatian_ci DEFAULT NULL,
  `logo` varchar(200) COLLATE utf8mb4_croatian_ci DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=24 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_croatian_ci;

-- ----------------------------
-- Records of equip_brands
-- ----------------------------
BEGIN;
INSERT INTO `equip_brands` VALUES (1, '2018-09-18 14:16:29', '2018-09-18 14:16:31', NULL, 'Apple', NULL);
INSERT INTO `equip_brands` VALUES (2, '2018-09-18 14:17:09', '2018-09-18 14:17:11', NULL, 'Dell', NULL);
INSERT INTO `equip_brands` VALUES (3, '2018-09-18 14:18:31', '2018-09-18 14:18:33', NULL, 'HP', NULL);
INSERT INTO `equip_brands` VALUES (4, '2018-09-18 14:18:45', '2018-09-18 14:18:48', NULL, 'ThinkPad', NULL);
INSERT INTO `equip_brands` VALUES (5, '2018-09-18 14:19:23', '2018-09-18 14:19:25', NULL, 'Asus', NULL);
INSERT INTO `equip_brands` VALUES (6, '2018-09-18 14:21:23', '2018-09-18 14:21:26', NULL, 'MagicBook', NULL);
INSERT INTO `equip_brands` VALUES (7, '2018-09-18 14:25:11', '2018-09-18 14:25:13', NULL, 'Lenovo', NULL);
INSERT INTO `equip_brands` VALUES (8, '2018-09-18 14:25:39', '2018-09-18 14:25:41', NULL, 'MI', NULL);
INSERT INTO `equip_brands` VALUES (9, '2018-09-18 14:26:08', '2018-09-18 14:26:10', NULL, 'Acer', NULL);
INSERT INTO `equip_brands` VALUES (10, '2018-09-18 14:26:25', '2018-09-18 14:26:27', NULL, 'Samsung', NULL);
INSERT INTO `equip_brands` VALUES (11, '2018-09-18 15:16:56', '2018-09-18 15:16:58', NULL, '华为', NULL);
INSERT INTO `equip_brands` VALUES (12, '2018-09-18 15:17:28', '2018-09-18 15:17:31', NULL, '荣耀', NULL);
INSERT INTO `equip_brands` VALUES (13, '2018-09-18 15:17:40', '2018-09-18 15:17:42', NULL, 'VIVO', NULL);
INSERT INTO `equip_brands` VALUES (14, '2018-09-18 15:18:14', '2018-09-18 15:18:16', NULL, 'OPPO', NULL);
INSERT INTO `equip_brands` VALUES (15, '2018-09-18 15:18:38', '2018-09-18 15:18:40', NULL, '魅族', NULL);
INSERT INTO `equip_brands` VALUES (16, '2018-09-18 15:18:50', '2018-09-18 15:18:52', NULL, '一加', NULL);
INSERT INTO `equip_brands` VALUES (17, '2018-09-18 15:19:34', '2018-09-18 15:19:36', NULL, '锤子', NULL);
INSERT INTO `equip_brands` VALUES (18, '2018-09-18 15:26:24', '2018-09-18 15:26:25', NULL, 'Nubia', NULL);
INSERT INTO `equip_brands` VALUES (19, '2018-09-18 15:26:59', '2018-09-18 15:27:02', NULL, 'Nokia', NULL);
INSERT INTO `equip_brands` VALUES (20, '2018-09-18 15:27:13', '2018-09-18 15:27:16', NULL, 'ZTE', NULL);
INSERT INTO `equip_brands` VALUES (21, '2018-09-18 15:28:39', '2018-09-18 15:28:41', NULL, 'MOTO', NULL);
INSERT INTO `equip_brands` VALUES (22, '2018-09-18 15:31:37', '2018-09-18 15:31:43', NULL, 'LG', NULL);
INSERT INTO `equip_brands` VALUES (23, '2018-09-18 15:32:10', '2018-09-18 15:32:12', NULL, 'HTC', NULL);
COMMIT;

-- ----------------------------
-- Table structure for equip_categorys
-- ----------------------------
DROP TABLE IF EXISTS `equip_categorys`;
CREATE TABLE `equip_categorys` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `name` varchar(50) COLLATE utf8mb4_croatian_ci NOT NULL,
  `icon` text COLLATE utf8mb4_croatian_ci,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_croatian_ci;

-- ----------------------------
-- Records of equip_categorys
-- ----------------------------
BEGIN;
INSERT INTO `equip_categorys` VALUES (1, '2018-09-18 14:07:07', '2018-09-18 14:07:10', NULL, '电脑', NULL);
INSERT INTO `equip_categorys` VALUES (2, '2018-09-18 14:07:26', '2018-09-18 14:07:28', NULL, '手机', NULL);
INSERT INTO `equip_categorys` VALUES (3, '2018-09-18 14:08:29', '2018-09-18 14:08:31', NULL, '键盘', NULL);
INSERT INTO `equip_categorys` VALUES (4, '2018-09-18 14:08:42', '2018-09-18 14:08:44', NULL, '鼠标', NULL);
INSERT INTO `equip_categorys` VALUES (5, '2018-09-18 15:22:27', '2018-09-18 15:22:30', NULL, '耳机', NULL);
COMMIT;

-- ----------------------------
-- Table structure for equipments
-- ----------------------------
DROP TABLE IF EXISTS `equipments`;
CREATE TABLE `equipments` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `cate_id` bigint(20) DEFAULT NULL,
  `brand_id` bigint(20) DEFAULT NULL,
  `name` varchar(200) COLLATE utf8mb4_croatian_ci DEFAULT NULL,
  `models` varchar(200) COLLATE utf8mb4_croatian_ci DEFAULT NULL COMMENT '型号',
  `attributes` text COLLATE utf8mb4_croatian_ci COMMENT '其他属性',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_croatian_ci;

-- ----------------------------
-- Table structure for git_repos
-- ----------------------------
DROP TABLE IF EXISTS `git_repos`;
CREATE TABLE `git_repos` (
  `id` bigint(20) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `repo_id` bigint(20) DEFAULT NULL COMMENT 'repo的id',
  `name` varchar(100) COLLATE utf8mb4_croatian_ci DEFAULT NULL COMMENT 'repo name',
  `full_name` varchar(100) COLLATE utf8mb4_croatian_ci DEFAULT NULL COMMENT 'full name',
  `owner_id` bigint(20) DEFAULT NULL COMMENT 'owner id',
  `html_url` varchar(200) COLLATE utf8mb4_croatian_ci DEFAULT NULL COMMENT '项目首页',
  `description` varchar(400) COLLATE utf8mb4_croatian_ci DEFAULT NULL COMMENT '项目描述',
  `fork` tinyint(1) DEFAULT NULL COMMENT '是否fork',
  `api_url` varchar(200) COLLATE utf8mb4_croatian_ci DEFAULT NULL COMMENT '项目的API地址',
  `assignees_url` varchar(200) COLLATE utf8mb4_croatian_ci DEFAULT NULL COMMENT 'assignees_url',
  `blobs_url` varchar(200) COLLATE utf8mb4_croatian_ci DEFAULT NULL COMMENT 'blobs_url',
  `branches_url` varchar(200) COLLATE utf8mb4_croatian_ci DEFAULT NULL COMMENT 'branches_url',
  `collaborators_url` varchar(200) COLLATE utf8mb4_croatian_ci DEFAULT NULL COMMENT 'collaborators_url',
  `comments_url` varchar(200) COLLATE utf8mb4_croatian_ci DEFAULT NULL COMMENT 'comments_url',
  `commits_url` varchar(200) COLLATE utf8mb4_croatian_ci DEFAULT NULL COMMENT 'commits_url',
  `contributors_url` varchar(200) COLLATE utf8mb4_croatian_ci DEFAULT NULL COMMENT 'contributors_url',
  `deployments_url` varchar(200) COLLATE utf8mb4_croatian_ci DEFAULT NULL COMMENT 'deployments_url',
  `downloads_url` varchar(200) COLLATE utf8mb4_croatian_ci DEFAULT NULL COMMENT 'downloads_url',
  `forks_url` varchar(200) COLLATE utf8mb4_croatian_ci DEFAULT NULL COMMENT 'forks_url',
  `git_url` varchar(200) COLLATE utf8mb4_croatian_ci DEFAULT NULL COMMENT 'git_url',
  `issue_comment_url` varchar(200) COLLATE utf8mb4_croatian_ci DEFAULT NULL COMMENT 'issue_comment_url',
  `issues_url` varchar(200) COLLATE utf8mb4_croatian_ci DEFAULT NULL COMMENT 'issues_url',
  `labels_url` varchar(200) COLLATE utf8mb4_croatian_ci DEFAULT NULL COMMENT 'labels_url',
  `languages_url` varchar(200) COLLATE utf8mb4_croatian_ci DEFAULT NULL COMMENT 'languages_url',
  `pulls_url` varchar(200) COLLATE utf8mb4_croatian_ci DEFAULT NULL COMMENT 'pulls_url',
  `releases_url` varchar(200) COLLATE utf8mb4_croatian_ci DEFAULT NULL COMMENT 'releases_url',
  `stargazers_url` varchar(200) COLLATE utf8mb4_croatian_ci DEFAULT NULL COMMENT 'stargazers_url',
  `statuses_url` varchar(200) COLLATE utf8mb4_croatian_ci DEFAULT NULL COMMENT 'statuses_url',
  `subscribers_url` varchar(200) COLLATE utf8mb4_croatian_ci DEFAULT NULL COMMENT 'subscribers_url',
  `subscription_url` varchar(200) COLLATE utf8mb4_croatian_ci DEFAULT NULL COMMENT 'subscription_url',
  `tags_url` varchar(200) COLLATE utf8mb4_croatian_ci DEFAULT NULL COMMENT 'tags_url',
  `teams_url` varchar(200) COLLATE utf8mb4_croatian_ci DEFAULT NULL COMMENT 'teams_url',
  `stargazers_count` int(11) DEFAULT NULL COMMENT 'stars，stargazers_count',
  `watchers_count` int(11) DEFAULT NULL COMMENT 'watchers， watchers_count',
  `forks_count` int(11) DEFAULT NULL COMMENT 'forks',
  `language` varchar(20) COLLATE utf8mb4_croatian_ci DEFAULT NULL COMMENT '开发语言。',
  `primary_language` varchar(0) COLLATE utf8mb4_croatian_ci DEFAULT NULL COMMENT '主要语言',
  `category` varchar(0) COLLATE utf8mb4_croatian_ci DEFAULT NULL COMMENT '项目所属分类。Library, Framework, 等',
  `code_lines` int(11) DEFAULT NULL COMMENT '代码行数',
  `size` int(11) DEFAULT NULL COMMENT '应该是文件数量',
  `open_issues_count` int(11) DEFAULT NULL COMMENT '未关闭的issue数量',
  `network_count` int(11) DEFAULT NULL COMMENT 'network_count',
  `subscribers_count` int(11) DEFAULT NULL COMMENT 'subscribers_count',
  `pushed_at` timestamp NULL DEFAULT NULL COMMENT 'pushed_at',
  `license` varchar(0) COLLATE utf8mb4_croatian_ci DEFAULT NULL COMMENT 'license > name',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_croatian_ci COMMENT='git (github, gitlab等) 仓库';

-- ----------------------------
-- Table structure for github_users
-- ----------------------------
DROP TABLE IF EXISTS `github_users`;
CREATE TABLE `github_users` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT COMMENT '自增ID',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `linked_user` bigint(20) DEFAULT NULL COMMENT '与本系统users表关联的用户id',
  `user_id` bigint(20) DEFAULT NULL COMMENT 'github 的userid',
  `login` varchar(60) COLLATE utf8mb4_croatian_ci DEFAULT NULL COMMENT '登录账号',
  `avatar_url` varchar(200) COLLATE utf8mb4_croatian_ci DEFAULT NULL COMMENT '头像, gravatar_id avatar_url 之一',
  `html_url` varchar(200) COLLATE utf8mb4_croatian_ci DEFAULT NULL COMMENT '个人首页',
  `repos_url` varchar(200) COLLATE utf8mb4_croatian_ci DEFAULT NULL COMMENT '仓库列表',
  `api_url` varchar(200) COLLATE utf8mb4_croatian_ci DEFAULT NULL COMMENT 'API个人首页，url 字段',
  `subscriptions_url` varchar(200) COLLATE utf8mb4_croatian_ci DEFAULT NULL COMMENT 'subscriptions_url',
  `organizations_url` varchar(200) COLLATE utf8mb4_croatian_ci DEFAULT NULL COMMENT 'organizations_url',
  `followers_url` varchar(200) COLLATE utf8mb4_croatian_ci DEFAULT NULL COMMENT 'follower',
  `following_url` varchar(200) COLLATE utf8mb4_croatian_ci DEFAULT NULL COMMENT 'following_url',
  `gists_url` varchar(200) COLLATE utf8mb4_croatian_ci DEFAULT NULL COMMENT 'gists_url',
  `type` varchar(50) COLLATE utf8mb4_croatian_ci DEFAULT NULL COMMENT '类型,User, Orginazation',
  `site_admin` tinyint(1) DEFAULT NULL COMMENT 'site_admin',
  `public_repos` int(11) DEFAULT NULL COMMENT 'public_repos',
  `public_gists` int(11) DEFAULT NULL COMMENT 'public_gists',
  `followers` int(11) DEFAULT NULL COMMENT 'followers',
  `following` int(11) DEFAULT NULL COMMENT 'following',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_croatian_ci;

-- ----------------------------
-- Table structure for sequences
-- ----------------------------
DROP TABLE IF EXISTS `sequences`;
CREATE TABLE `sequences` (
  `name` varchar(100) COLLATE utf8mb4_croatian_ci NOT NULL COMMENT '序列名称',
  `value` int(11) DEFAULT NULL COMMENT '当前序列的值',
  PRIMARY KEY (`name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_croatian_ci;

-- ----------------------------
-- Table structure for users
-- ----------------------------
DROP TABLE IF EXISTS `users`;
CREATE TABLE `users` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `username` varchar(100) COLLATE utf8mb4_croatian_ci DEFAULT NULL,
  `email` varchar(100) COLLATE utf8mb4_croatian_ci DEFAULT NULL,
  `mobile` varchar(20) COLLATE utf8mb4_croatian_ci DEFAULT NULL,
  `name` varchar(100) COLLATE utf8mb4_croatian_ci DEFAULT NULL,
  `avatar` varchar(255) COLLATE utf8mb4_croatian_ci DEFAULT NULL,
  `password` varchar(40) COLLATE utf8mb4_croatian_ci DEFAULT NULL,
  `salt` varchar(40) COLLATE utf8mb4_croatian_ci DEFAULT NULL,
  `lang` varchar(40) COLLATE utf8mb4_croatian_ci DEFAULT NULL,
  `register_from` varchar(40) COLLATE utf8mb4_croatian_ci DEFAULT NULL,
  `status` varchar(40) COLLATE utf8mb4_croatian_ci DEFAULT NULL,
  `third_party_user_id` varchar(100) COLLATE utf8mb4_croatian_ci DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_croatian_ci;

SET FOREIGN_KEY_CHECKS = 1;
