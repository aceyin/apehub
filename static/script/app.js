// core script
const Apehub = {
    user: function () {
        let cookie = Cookies.get("user");
        if (cookie === "" || cookie === undefined) return {};
        let u = Base64.decode(cookie);
        if (u === "" || u === undefined) return {};
        return u;
    }
};