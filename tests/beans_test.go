package tests

import (
	"testing"
	"io/ioutil"
	"encoding/json"
	"apehub/domain"
	"apehub/resp"
)

func Test_GithubBeansUnmarshal(t *testing.T) {
	bytes, e := ioutil.ReadFile("../docs/github-user.json")
	if e != nil {
		t.Fatal("读取文件出错")
		t.Fail()
	}

	users := new(domain.GithubUser)
	json.Unmarshal(bytes, users)

	println("users = ", users.Name)
}

func Test_type_conversion(t *testing.T) {
	user := domain.User{
		Name:         "Nick name",
		Username:     "username",
		Avatar:       "avatar",
		Password:     "password",
		Salt:         "salt",
		Lang:         "lang",
		RegisterFrom: "regist from",
		Status:       "Status",
	}

	ok := resp.Success("msg", user)
	e := ok.Data.(domain.User)

	println(e.Name)

}
