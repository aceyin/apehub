package service

import "apehub/domain"

var SequenceService = struct {
	Get func(key string, def string) string
	Set func(key string, value string)
}{
	Get: func(key string, def string) string {
		seq := domain.Sequences{
			Name:  key,
			Value: def,
		}
		database.FirstOrCreate(&seq, "name=?", key)
		return seq.Value
	},
	Set: func(key string, value string) {
		seq := domain.Sequences{Name: key, Value: value}
		database.Where(domain.Sequences{Name: key}).Assign(domain.Sequences{Value: value}).FirstOrCreate(&seq)
	},
}
