package core

import (
	"github.com/gin-gonic/gin"
	"fmt"
)

func CookieMiddleware() gin.HandlerFunc {
	return func(c *gin.Context) {
		//cookie, e := c.Cookie(consts.UserCookieName)

		fmt.Println("Cookie middleware called")
		c.Next()
	}
}

func ResponseMiddleware(c *gin.Context) {
	fmt.Println("ResponseMiddleware called")
}
